#include <stdio.h>
#include <stdlib.h>

void lerValores(int *vetor, int n);

int main(){
    int *valores, n, j;

    printf("Informe o tamanho do vetor: ");
    scanf("%d", &n);
    valores = malloc(n*sizeof(int));

    if(!valores){
        printf("Falha na alocacao de memoria.");
        exit(1);
    }

    lerValores(valores, n);
    
	printf("Valores informados:\n ");
	
    for(j=0;j<n;j++){
    
    	printf("%d ", valores[j]);
    }

    free(valores);

    return(0);
}

void lerValores(int *vetor, int n){
    int j;
    for(j=0;j<n;j++){
        printf("Elemento %d: ", j);
        scanf("%d", &vetor[j]);
    }
}
