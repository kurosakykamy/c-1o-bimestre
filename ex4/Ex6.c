#include <stdio.h>

int tigre_dente_de_sabre(int x){
if(x == 0)return 1;
return x*tigre_dente_de_sabre(x-1);
}

int main(){
int n;

printf("Informe um numero inteiro:");
scanf("%d",&n);

printf("\nO fatorial de %d = %d\n\n",n,tigre_dente_de_sabre(n));

system("pause");

return 0;
}
